"""Tests for the elevator code."""
import unittest

import main


class ElevatorTest(unittest.TestCase):
    """Tests for the elevator code."""

    def test_fake_method(self):
        """Just some example to help you start writing tests."""
        self.assertEqual(1, main.fake_method())


if __name__ == '__main__':
    unittest.main()
