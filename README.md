# Wave Coding Exercise

Install docker and docker-compose.

## Python
### Build and run
```
docker-compose build python
docker-compose run --rm python bash
```
Once your are in the bash of the Docker container (`root@1234456789abc/app#`):

To run tests:
```
nosetests --with-watch
```
To run tests while showing your `print()` statements in the code:
```
nosetests --with-watch --nocapture
```
If for any reason you want to run your main script:
```
python src/main.py
```

## JS
### To run tests
The default Docker command is to run tests with "watch"
```
docker-compose build js
docker-compose up js
```