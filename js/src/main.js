const Elevator = require("./models/Elevator");
const ElevatorState = require("./models/ElevatorState");
const UserState = require("./models/UserState");
const Direction = require("./models/Direction");
const userGenerator = require("./user-generator");

// Main file for coding exercise.

// ----------------------------------------------------------------------------
// Simulator
// ----------------------------------------------------------------------------
const DEFAULT_RUN_OPTIONS = {
  lowestFloor: 0,
  highestFloor: 10,
  elevatorCount: 3,
  stepCount: 1000,
  userGenerationInterval: 30,
  userGenerationMin: 1,
  userGenerationMax: 5,
  userGenerationHandler: userGenerator.linear,
  onTurnEnd: () => {},
};

function run(nextTargetHandler, options = DEFAULT_RUN_OPTIONS) {
  // Init
  const elevators = [];
  for (let i = 0; i < options.elevatorCount; ++i) {
    elevators.push(new Elevator(i, options.lowestFloor, options.highestFloor));
  }
  let users = [];
  let delta = (options.highestFloor - options.lowestFloor) * 8;
  // Add delta to allow last generated users to leave
  for (let time = 0; time < options.stepCount + delta; ++time) {
    // Elevators updates
    elevators.forEach((e) => e.update());

    // Users updates
    users.forEach((u) => u.update(time, elevators));

    // Retrieve idle elevators and floor requests
    const idleElevators = elevators.filter(
      (e) => e.state === ElevatorState.Idle
    );

    // Decide elevators next targets
    nextTargetHandler(idleElevators, users);

    // New users generation
    // Stop at max step
    if (
      time <= options.stepCount &&
      time % options.userGenerationInterval === 0
    ) {
      const newUsers = options.userGenerationHandler(
        time,
        options.userGenerationMin,
        options.userGenerationMax,
        options.lowestFloor,
        options.highestFloor
      );
      users = users.concat(newUsers);
    }
  }

  // Stats
  let goneUsers = users.filter((u) => u.state === UserState.Gone);
  let waitingUsers = users.filter((u) => u.state === UserState.Waiting);
  let boardedUsers = users.filter((u) => u.state === UserState.Boarded);

  let elevatorsBoardedCount = users
    .filter((u) => u.boardedElevator !== null)
    .map((u) => u.boardedElevator.id)
    .reduce((prev, curr) => {
      if (prev[curr] === undefined) {
        prev[curr] = 0;
      } else {
        ++prev[curr];
      }
      return prev;
    }, []);

  const totals = goneUsers
    .map((u) => ({
      travelTime: u.goneAt - u.createdAt,
      waitingTime: u.boardedAt - u.createdAt,
    }))
    .reduce(
      (prev, curr) => {
        return {
          totalTravelTime: prev.totalTravelTime + curr.travelTime,
          totalWaitingTime: prev.totalWaitingTime + curr.waitingTime,
        };
      },
      { totalTravelTime: 0, totalWaitingTime: 0 }
    );

  return {
    total: users.length,
    gone: goneUsers.length,
    waiting: waitingUsers.length,
    boarded: boardedUsers.length,
    meanTravelTime: totals.totalTravelTime / goneUsers.length,
    meanWaitingTime: totals.totalWaitingTime / goneUsers.length,
    elevatorsBoardedCount,
  };
}

// ----------------------------------------------------------------------------
// Decision function
// ----------------------------------------------------------------------------
function elevatorsTargetHandler(idleElevators, users) {
  // Extract users data
  const waitingUsers = users.filter((u) => u.state === UserState.Waiting);

  // Floors where people are going down
  const goingDownFloors = waitingUsers
    .filter((u) => u.direction === Direction.Down)
    .map((u) => u.waitingFloor)
    .reduce((prev, curr) => {
      return prev.indexOf(curr) === -1 ? prev.concat(curr) : prev;
    }, [])
    .sort((a, b) => b - a);

  // Floor where people are going up
  const goingUpFloors = waitingUsers
    .filter((u) => u.direction === Direction.Up)
    .map((u) => u.waitingFloor)
    .reduce((prev, curr) => {
      return prev.indexOf(curr) === -1 ? prev.concat(curr) : prev;
    }, [])
    .sort((a, b) => a - b);

  idleElevators.forEach((e) => {
    // Request inside elevator (made by users)
    // concat with request outside
    const requestedAbove = e.requestedFloors
      .filter((r) => r > e.currentFloor)
      .concat(goingUpFloors)
      .sort((a, b) => a - b);
    const requestedBelow = e.requestedFloors
      .filter((r) => r < e.currentFloor)
      .concat(goingDownFloors)
      .sort((a, b) => b - a);

    if (e.requestedFloors.indexOf(e.nextFloor) !== -1) {
      e.targetFloor = e.nextFloor;
    } else if (e.direction === Direction.Up && requestedAbove.length > 0) {
      e.targetFloor = requestedAbove[0];
      let index = goingUpFloors.indexOf(requestedAbove[0]);
      if (index !== -1) {
        goingUpFloors.splice(index, 1);
      }
    } else if (e.direction === Direction.Up && requestedBelow.length > 0) {
      e.targetFloor = requestedBelow[0];
      let index = goingDownFloors.indexOf(requestedBelow[0]);
      if (index !== -1) {
        goingDownFloors.splice(index, 1);
      }
    } else if (e.direction === Direction.Down && requestedBelow.length > 0) {
      e.targetFloor = requestedBelow[0];
      let index = goingDownFloors.indexOf(requestedBelow[0]);
      if (index !== -1) {
        goingDownFloors.splice(index, 1);
      }
    } else if (e.direction === Direction.Down && requestedAbove.length > 0) {
      e.targetFloor = requestedAbove[0];
      let index = goingUpFloors.indexOf(requestedAbove[0]);
      if (index !== -1) {
        goingUpFloors.splice(index, 1);
      }
    }
  });
}

/*

*/

module.exports = {
  run,
  elevatorsTargetHandler,
  DEFAULT_RUN_OPTIONS,
};
