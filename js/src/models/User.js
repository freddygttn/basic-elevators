const ElevatorState = require("./ElevatorState");
const UserState = require("./UserState");
const Direction = require("./Direction");

class User {
  constructor(waitingFloor, targetFloor, createdAt) {
    this.state = UserState.Waiting;
    this.waitingFloor = waitingFloor;
    this.targetFloor = targetFloor;
    this.createdAt = createdAt;
    this.boardedElevator = null;
    this.boardedAt = null;
    this.goneAt = null;
  }

  get isWaiting() {
    return this.state === UserState.Waiting;
  }

  get isMoving() {
    return this.state === UserState.Boarded;
  }

  get isGone() {
    return this.state === UserState.Gone;
  }

  get direction() {
    // don't care about equality
    return this.targetFloor < this.waitingFloor ? Direction.Down : Direction.Up;
  }

  // --------------------------------------------------------------------------
  update(time, elevators) {
    if (this.state === UserState.Waiting) {
      const availableElevators = elevators.filter(
        (e) =>
          e.state === ElevatorState.Idle &&
          e.currentFloor === this.waitingFloor &&
          e.direction === this.direction
      );
      if (availableElevators.length > 0) {
        this.state = UserState.Boarded;
        this.boardedElevator = availableElevators[0];
        this.boardedElevator.addRequestedFloor(this.targetFloor);
        this.boardedAt = time;
      }
    } else if (
      this.state === UserState.Boarded &&
      this.boardedElevator !== null &&
      this.boardedElevator.currentFloor === this.targetFloor &&
      this.boardedElevator.state === ElevatorState.Idle
    ) {
      this.state = UserState.Gone;
      this.goneAt = time;
    }
  }
}

module.exports = User;
