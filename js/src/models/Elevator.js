const ElevatorState = require("./ElevatorState");
const Direction = require("./Direction");

class Elevator {
  constructor(id, lowestFloor, highestFloor) {
    this.id = id;
    this.state = ElevatorState.Idle;
    this.lowestFloor = lowestFloor;
    this.highestFloor = highestFloor;
    this.previousFloor = 0;
    this.currentFloor = 0;
    this.nextFloor = 0;
    this.targetFloor = 0;
    this.requestedFloors = [];
  }

  get isIdle() {
    return this.state === ElevatorState.Idle;
  }

  get isBusy() {
    return this.state === ElevatorState.Busy;
  }

  get direction() {
    if (this.currentFloor === this.highestFloor) {
      return Direction.Down;
    }
    if (this.currentFloor === this.lowestFloor) {
      return Direction.Up;
    }

    return this.previousFloor < this.currentFloor
      ? Direction.Up
      : Direction.Down;
  }

  addRequestedFloor(target) {
    if (this.requestedFloors.indexOf(target) === -1) {
      this.requestedFloors.push(target);
    }
  }

  // --------------------------------------------------------------------------
  update() {
    if (this.targetFloor < this.currentFloor) {
      // Descending
      this.previousFloor = this.currentFloor;
      this.currentFloor = this.currentFloor - 1;
      this.nextFloor =
        this.currentFloor === this.lowestFloor
          ? this.previousFloor
          : this.currentFloor - 1;
      this.state = ElevatorState.Busy;
    } else if (this.targetFloor > this.currentFloor) {
      // Ascending
      this.previousFloor = this.currentFloor;
      this.currentFloor = this.currentFloor + 1;
      this.nextFloor =
        this.currentFloor === this.highestFloor
          ? this.previousFloor
          : this.currentFloor + 1;
      this.state = ElevatorState.Busy;
    } else {
      // Idle
      this.requestedFloors = this.requestedFloors.filter(
        (f) => f !== this.currentFloor
      );
      this.state = ElevatorState.Idle;
    }
  }
}

module.exports = Elevator;
