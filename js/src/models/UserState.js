module.exports = {
  Waiting: "@USER_STATE_WAITING",
  Boarded: "@USER_STATE_BOARDED",
  Gone: "@USER_STATE_GONE",
};
