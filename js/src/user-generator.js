const User = require("./models/User");

function randomIntBetween(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function fixedFloor(time, min, max, lowestFloor, highestFloor) {
  const newUsers = [];
  let newCount = randomIntBetween(min, max);
  for (let i = 0; i < newCount; ++i) {
    newUsers.push(new User(highestFloor, lowestFloor, time));
  }

  return newUsers;
}

function linear(time, min, max, lowestFloor, highestFloor) {
  const newUsers = [];
  let newCount = randomIntBetween(min, max);
  for (let i = 0; i < newCount; ++i) {
    let waitingFloor = randomIntBetween(lowestFloor, highestFloor);
    let targetFloor = randomIntBetween(lowestFloor, highestFloor);
    while (targetFloor === waitingFloor) {
      targetFloor = randomIntBetween(lowestFloor, highestFloor);
    }
    newUsers.push(new User(waitingFloor, targetFloor, time));
  }

  return newUsers;
}

module.exports = {
  fixedFloor,
  linear,
};
