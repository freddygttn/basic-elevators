const expect = require("chai").expect;
const ElevatorState = require("../src/models/ElevatorState");
const UserState = require("../src/models/UserState");
const Elevator = require("../src/models/Elevator");
const User = require("../src/models/User");

describe("User.update", function () {
  it("puts the user in an available elevator", function () {
    const elevator = new Elevator();
    elevator.previousFloor = 2;
    elevator.currentFloor = 1;
    elevator.state = ElevatorState.Idle;
    const user = new User(1, 0, 0);
    user.state = UserState.Waiting;
    const updateTime = 1;
    user.update(updateTime, [elevator]);
    expect(user.state).to.equal(UserState.Boarded);
    expect(user.boardedElevator).to.equal(elevator);
    expect(user.boardedAt).to.equal(updateTime);
    expect(elevator.requestedFloors.indexOf(user.targetFloor)).to.not.equal(-1);
  });

  it("sets the user as gone when elevator reached target floor", function () {
    const elevator = new Elevator();
    elevator.previousFloor = 1;
    elevator.currentFloor = 0;
    elevator.state = ElevatorState.Idle;
    const user = new User(1, 0, 0);
    user.state = UserState.Boarded;
    user.boardedElevator = elevator;
    const updateTime = 1;
    user.update(updateTime, [elevator]);
    expect(user.state).to.equal(UserState.Gone);
    expect(user.goneAt).to.equal(updateTime);
  });
});
