const expect = require("chai").expect;
const ElevatorState = require("../src/models/ElevatorState");
const Elevator = require("../src/models/Elevator");

describe("Elevator.update", function () {
  it("updates the currentFloor if targetFloor is not reached", function () {
    const elevator = new Elevator();
    elevator.currentFloor = 0;
    elevator.targetFloor = 1;
    const currentDiff = Math.abs(elevator.targetFloor - elevator.currentFloor);
    elevator.update();
    const newDiff = Math.abs(elevator.targetFloor - elevator.currentFloor);
    expect(newDiff).to.be.below(currentDiff);
  });

  it("sets the busy state when elevator is moving", function () {
    const elevator = new Elevator();
    elevator.currentFloor = 0;
    elevator.targetFloor = 1;
    elevator.state = ElevatorState.Idle;
    elevator.update();
    expect(elevator.state).to.equal(ElevatorState.Busy);
  });

  it("does not update the currentFloor and remove request if targetFloor reached", function () {
    const elevator = new Elevator();
    const currentFloor = 0;
    elevator.currentFloor = currentFloor;
    elevator.targetFloor = currentFloor;
    elevator.requestedFloors = [currentFloor];
    elevator.update();
    expect(elevator.currentFloor).to.equal(currentFloor);
    expect(elevator.requestedFloors.length).to.equal(0);
  });

  it("sets the idle state when elevator is not moving", function () {
    const elevator = new Elevator();
    elevator.currentFloor = 0;
    elevator.targetFloor = 0;
    elevator.state = ElevatorState.Busy;
    elevator.update();
    expect(elevator.state).to.equal(ElevatorState.Idle);
  });
});
