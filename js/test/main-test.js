const expect = require("chai").expect;
const main = require("../src/main");
const userGenerator = require("../src/user-generator");

describe("elevatorsTargetHandler", function () {
  it("should handle user requests", function () {
    const defaultStats = main.run(
      main.elevatorsTargetHandler,
      main.DEFAULT_RUN_OPTIONS
    );
    expect(defaultStats.gone).to.be.above(0);
  });

  it("run test 1 - 10 floors, 3 elevators, normal", function () {
    const defaultStats = main.run(
      main.elevatorsTargetHandler,
      main.DEFAULT_RUN_OPTIONS
    );

    console.log(defaultStats);
  });

  it("run test 2 - 10 floors, 1 elevator - normal", function () {
    const options = {
      ...main.DEFAULT_RUN_OPTIONS,
      elevatorCount: 1,
    };
    const defaultStats = main.run(main.elevatorsTargetHandler, options);

    console.log(defaultStats);
  });

  it("run test 3 - 10 floors, 3 elevators - fixed floor", function () {
    const options = {
      ...main.DEFAULT_RUN_OPTIONS,
      userGenerationHandler: userGenerator.fixedFloor,
    };
    const defaultStats = main.run(main.elevatorsTargetHandler, options);

    console.log(defaultStats);
  });

  it("run test 4 - 10 floors, 3 elevators - busy", function () {
    const options = {
      ...main.DEFAULT_RUN_OPTIONS,
      userGenerationInterval: 3,
      userGenerationMin: 3,
      userGenerationMax: 12,
    };
    const defaultStats = main.run(main.elevatorsTargetHandler, options);
    console.log(defaultStats);
  });

  it("run test 5 - 10 floors, 5 elevators - busy", function () {
    const options = {
      ...main.DEFAULT_RUN_OPTIONS,
      elevatorCount: 5,
      userGenerationInterval: 3,
      userGenerationMin: 3,
      userGenerationMax: 12,
    };
    const defaultStats = main.run(main.elevatorsTargetHandler, options);
    console.log(defaultStats);
  });
  it("run test 5 - 20 floors, 5 elevators - busy", function () {
    const options = {
      ...main.DEFAULT_RUN_OPTIONS,
      highestFloor: 20,
      elevatorCount: 5,
      userGenerationInterval: 3,
      userGenerationMin: 3,
      userGenerationMax: 12,
    };
    const defaultStats = main.run(main.elevatorsTargetHandler, options);
    console.log(defaultStats);
  });
});
